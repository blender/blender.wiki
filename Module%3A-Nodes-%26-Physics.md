## Links

[Workboard](https://projects.blender.org/blender/blender/projects/9) | [Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=275%2c296) | [High Severity](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=275%2c285) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=275%2c298)


## Members
* Module owners:  [Jacques Lucke](https://projects.blender.org/JacquesLucke), [Dalai Felinto](https://projects.blender.org/dfelinto)
* Members: [Jacques Lucke](https://projects.blender.org/JacquesLucke), [Hans Goudey](https://projects.blender.org/HooglyBoogly), [Lukas Tönne](https://projects.blender.org/LukasTonne), [Johnny Matthews](https://projects.blender.org/guitargeek), [Erik Abrahamsson](https://projects.blender.org/erik85)
* Artists: [Simon Thommes](https://projects.blender.org/SimonThommes), [Erindale Woodford](https://projects.blender.org/Erindale),  [Miro Horvath](https://projects.blender.org/MiroHorvath)

## Communication
* Chat: [#nodes-physics-module](https://chat.blender.org/#/room/#module-nodes-physics:blender.org)
* Forum: [Geometry Nodes](https://devtalk.blender.org/c/geometry-nodes/25)
* [Meeting Notes](https://devtalk.blender.org/tags/c/meetings/geometry-nodes)

## Documentation

* [Nodes & Physics](https://wiki.blender.org/wiki/Source#Nodes_.26_Physics)
* [Data Structures](https://developer.blender.org/docs/features/nodes/)


