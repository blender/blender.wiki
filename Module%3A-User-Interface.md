## Links

[Workboard](https://projects.blender.org/blender/blender/projects/16) | [Bugs](https://projects.blender.org/blender/blender/issues?type=all&state=open&labels=283&milestone=0&project=0&assignee=0) ([High Severity](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=285%2c283)) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=283%2c298)

## Description

This project includes user interface, Window Manager, UI drawing, internationalization, tools & operators, Outliner and more.

## Status

The module in maintenance mode. UI work necessary for the roadmaps of other projects and modules is still being done. There is not much capacity to review community contributions.
Experienced designers are needed.

## Members

Module owner: None

Coordinator: [Julian Eisel](https://projects.blender.org/JulianEisel)

Core UI team: [Brecht Van Lommel](https://projects.blender.org/brecht),  [Hans Goudey](https://projects.blender.org/HooglyBoogly), [Campbell Barton](https://projects.blender.org/ideasman42), [Julian Eisel](https://projects.blender.org/JulianEisel), [Pablo Vazquez](https://projects.blender.org/pablovazquez),   [Harley Acheson](https://projects.blender.org/harley), [Yevgeny Makarov](https://projects.blender.org/jenkm), [Guillermo Venegas](https://projects.blender.org/guishe), [Leon Schittek](https://projects.blender.org/lone_noel), [Julien Kaspar](https://projects.blender.org/JulienKaspar)

| Subproject                        | Developers                                |
| --------------------------------- | ----------------------------------------- |
| Window Manager                    | [Brecht Van Lommel](https://projects.blender.org/brecht), [Campbell Barton](https://projects.blender.org/ideasman42), [Julian Eisel](https://projects.blender.org/JulianEisel)  |
| Drawing & OpenGL                  | [Clément Foucault](https://projects.blender.org/fclem)  |
| Internationalization              | [Bastien Montagne](https://projects.blender.org/mont29)  |
| Tools & Operators |  [Campbell Barton](https://projects.blender.org/ideasman42), [Julian Eisel](https://projects.blender.org/JulianEisel) |
| Outliner | [Julian Eisel](https://projects.blender.org/JulianEisel), [Bastien Montagne](https://projects.blender.org/mont29) |

## Other Contributors

| Area  | Contributors  |
| --------------------------------- | ----------------------------------------- |
| Icons | [Andrzej Ambroz](https://projects.blender.org/jendrzych) |
| Mouse Cursors | Duarte Farrajota Ramos |
| IME Development & Testing | [Takahiro Shizuki](https://projects.blender.org/sntulix),  Shinsuke Irie, [Yuki Hashimoto](https://projects.blender.org/hzuika), [Kim Geonwoo](https://projects.blender.org/skyjib), xueke pei |
| Accessibility Testing | Kent Davis, @cmaury |

## Contacts

- [#module-user-interface](https://chat.blender.org/#/room/#module-user-interface:blender.org) on chat.blender.org
- [Forum](https://devtalk.blender.org/c/user-interface/23)
- Bug reports and patches are to be tagged with 'module > User Interface'
