## General information

This project is responsible for triaging bug reports, first round of Differential review.

As a general goal it would be desirable to have as little untriaged reports as possible (so important bugs dont slip through), that should be the main focus, Differential review comes second.

*   Module owners: [Philipp Oeser (lichtwerk)](https://projects.blender.org/lichtwerk), [Pratik Borhade (PratikPB2123)](https://projects.blender.org/PratikPB2123)
*   Module members: [Germano Cavalcante (mano-wii)](https://projects.blender.org/mano-wii), [Richard Antalik (ISS)](https://projects.blender.org/iss), [YimingWu (ChengduLittleA)](https://projects.blender.org/ChengduLittleA), [Alaska](https://projects.blender.org/Alaska)

## Status
The module is active.
Further streamlining the process (elaborating on triaging certain "hard-to-triage" reports and bringing playbook, manual and canned responses in line) is in process.

## Contacts

*   [#module-triaging](https://chat.blender.org/#/room/#module-triaging:blender.org) matrix chat room on chat.blender.org
*   [#module-triaging](https://matrix.to/#/#module-triaging:blender.org) matrix chat room link for other matrix clients

## Links

*   [Triaging Playbook](https://wiki.blender.org/wiki/Process/Bug_Reports/Triaging_Playbook)
*   [A Bugs Life](https://wiki.blender.org/wiki/Process/A_Bugs_Life)
*   [Help Triaging Bugs](https://wiki.blender.org/wiki/Process/Help_Triaging_Bugs)
*   [Effective Bug Reporting](https://wiki.blender.org/wiki/Process/Bug_Reports)
*   [Metrics](https://metrics.blender.org/d/PAPBaq0Vk/blender-bugs-reports-triaging-and-fix)
*   [Untriaged reports (blender repository)](https://projects.blender.org/blender/blender/issues?q=&type=all&state=open&labels=294)
*   [Reports waiting for information from Users (to be checked for activity after a week)](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=leastupdate&state=open&labels=293&milestone=0&project=0&assignee=0&poster=0)
*   [Reports waiting for information from Users (to be checked for activity after a week) -- own subscriptions](https://projects.blender.org/notifications/subscriptions?sort=leastupdate&state=open&labels=293,296)

*   [Confirmed, but Module tag missing](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=290%2c-284%2c-283%2c-282%2c-281%2c-280%2c-279%2c-278%2c-276%2c-275%2c-274%2c-273%2c-272%2c-270%2c-269%2c-268%2c-297%2c-299%2c-301&milestone=0&project=0&assignee=0&poster=0)
*   [Manual: troubleshooting crashes](https://docs.blender.org/manual/en/dev/troubleshooting/crash.html)
*   [Manual: troubleshooting on startup](https://docs.blender.org/manual/en/dev/troubleshooting/startup.html)
*   [Manual: troubleshooting other (includes crashes on startup)](https://docs.blender.org/manual/en/dev/troubleshooting/gpu/common/other.html)
*   [Manual: troubleshooting Graphics Hardware](https://docs.blender.org/manual/en/dev/troubleshooting/gpu/index.html)
*   [System requirements](https://www.blender.org/download/requirements/)
<!--*   [#bf\_blender:\_unconfirmed (tag to be used if report seems valid, but there is no way to reproduce)](https://developer.blender.org/project/view/50/)-->
*   [Overview of hardware in use by Blender developers (to get someone to reproduce in case report involves hardware not present)](https://developer.blender.org/docs/handbook/bug_reports/hardware_list/)
*   [List of issues involving unavailable hardware for testing](https://projects.blender.org/blender/blender/issues/126078)

