## Links

[Workboard](https://projects.blender.org/blender/blender/projects/6) | [Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=273%2c296) | [High Severity](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=273%2c285) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=273%2c298)

## Description

This project includes Grease Pencil drawing, editing, sculpting and all related to 2D animation module in Blender. Also contains the submodule #line_art related to generate grease pencil stroke drawing over 3D geometry (similar to FreeStyle).

Bug reports and patches are to be tagged with #grease_pencil

## Status
The module is active. You can see active tasks on the [workboard](https://projects.blender.org/blender/blender/projects/6) . If you want to get involved, contact us (info below).

## Blog
- [Grease Pencil 3.0](https://code.blender.org/2023/05/the-next-big-step-grease-pencil-3-0/)

## Members

* Module owners: @filedescriptor @pepe-school-land
* Artist members: @mendio @pepe-school-land
* Developers: @filedescriptor @antoniov

| Sub-project |Developers|
|---|---|
| Line Art | @ChengduLittleA |

## Contacts

- [#grease-pencil-module](https://chat.blender.org/#/room/#module-grease-pencil:blender.org) on chat.blender.org