Geometry Nodes is a sub-module of the [Nodes & Physics](https://projects.blender.org/blender/blender/projects/9) module, focused on procedural node based geometry editing and related projects.

# Communication
* Team chat on [#nodes-physics-module](https://blender.chat/channel/nodes-physics-module) on blender.chat.
* Meetings: https://devtalk.blender.org/tags/c/meetings/geometry-nodes

