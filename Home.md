### Modules

Blender development is organized into modules and projects with different teams.

#### General

|Module|Topics|
|---|---|
|[Core](https://projects.blender.org/blender/blender/wiki/Module:-Core)|DNA & RNA, .blend file, undo, datablocks, linking, overrides, support libraries|
|[Development Management](https://projects.blender.org/blender/blender/wiki/Module:-Development-Management)|Communication, release, documentation, forum, onboarding, infrastructure
|[Platforms, Builds, Tests & Devices](https://projects.blender.org/blender/blender/wiki/Module:-Platforms,-Builds,-Tests-&-Devices)|Windows, macOS, Linux, automated tests, build system, release builds, libraries|
|[Triaging](https://projects.blender.org/blender/blender/wiki/Module:-Triaging)|Triaging bug reports and first round of pull request review |
|[User Interface](https://projects.blender.org/blender/blender/wiki/Module:-User-Interface)|Interface, window manager, internationalization, tools & operators, outliner|

#### Features

|Module|Topics|
|---|---|
|Add-ons|[Official](https://projects.blender.org/blender/blender/wiki/Module:-Officially-Maintained-Add-ons) and [community](https://projects.blender.org/blender/blender/wiki/Module:-Community-Maintained-Add-ons) add-ons|
|[Animation & Rigging](https://projects.blender.org/blender/blender/wiki/Module:-Animation-&-Rigging)|Graph editor, dopespheet editor, NLA editor, keyframes, drivers, constraints, armatures|
|[Asset System](https://projects.blender.org/blender/blender/wiki/Module:-Asset-System)|Local and remote assets handling|
|[Grease Pencil](https://projects.blender.org/blender/blender/wiki/Module:-Grease-Pencil)|Grease pencil drawing, editing, sculpting and all related to 2D animation module in Blender |
|[Modeling](https://projects.blender.org/blender/blender/wiki/Module:-Modeling)|Meshes, modifiers, nurbs, curves, metaballs, transform, UV editor, subdivision surfaces|
|[Nodes & Physics](https://projects.blender.org/blender/blender/wiki/Module:-Nodes-&-Physics)|Geometry nodes, function nodes, node editor, simulations, rigid body, cloth, softbody, fluids|
|[Pipeline & I/O](https://projects.blender.org/blender/blender/wiki/Module:-Pipeline-&-I/O)|Import/export and integration into production pipelines|
|[Python API](https://projects.blender.org/blender/blender/wiki/Module:-Python-API-&-Text-Editor)|Python API, text editor and console|
|[EEVEE & Viewport](https://projects.blender.org/blender/blender/wiki/Module:-Viewport-&-EEVEE)|EEVEE, workbench, overlays, GPU, OpenGL, Vulkan, Metal, multi-view, virtual reality|
|[Render & Cycles](https://projects.blender.org/blender/blender/wiki/Module:-Render-&-Cycles)|Cycles, render pipeline, materials, textures, Freestyle, baking and color management |
|[Sculpt, Paint & Texture](https://projects.blender.org/blender/blender/wiki/Module:-Sculpt,-Paint-&-Texture)|Sculpting, vertex and image painting|
|[VFX & Video](https://projects.blender.org/blender/blender/wiki/Module:-VFX-&-Video)|Video sequencer, compositor, motion tracking, Libmv, audio|