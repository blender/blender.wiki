## Links

[Workboard](https://projects.blender.org/blender/blender/projects/5) | [Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=272%2c296) | [High Severity](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=272%2c285) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=272%2c298)

## Description

This project includes EEVEE, Workbench, Overlays, GPU, OpenGL, Vulkan, Multi-View, Virtual Reality and more.

## Status
The module is active. There are weekly private meetings and regular work on new features and improvements

## Members

|Role| |
|---|---|
|Module Owners| @fclem @Jeroen-Bakker @brecht|
|Developers| @OmarEmaraDev @pragma37 @weizhen|
|Artists| @juang3d @jonimercado @MetinSeven-1|

|Subproject|Developers|
|---|---|
|EEVEE, Workbench, Viewport, GPU|@fclem @Jeroen-Bakker|
|Virtual Reality |@JulianEisel @muxed-reality|

## Projects

> [!NOTE]
> - Expected target indicated the order that they should land, not a promise.

| Task                                                         | Ticket  | Expected target           |
| ------------------------------------------------------------ | ------- | ------------------------- |
| **Lead: Clement (@fclem)** 
| Removal of DST lock                                          |    #134690    | 4.5 Alpha                 |
| Add a shadow terminator bias                                 |         | 4.5 Alpha (after DST lock)|
| Optimize Dithered method shaders for shader compilation time |         | 4.5 Alpha (after DST lock)|
| Finish shader code cleanups                                  | #127983 | 4.5                       |
| Remove runtime parsing of GLSL source                        | #129009 | 4.5                       |
| EEVEE Reverse-Z                                              | #108123 | 4.5                       |
| **Lead: Miguel (@pragma37)**
| Material compilation cleanup                                 | #133674 | 4.5                       |
| Instancing optimization                                      | #130291 | 4.5                       |
| Format aliasing for efficient texture reuse                  | #112478 | 4.5                       |
| **Lead: Jeroen (@Jeroen-Bakker)**
| OpenSubdiv GPU Backend                                       | #133716 | 4.5                       |
| Performance target                                           | #130131 | 4.5                       |


## Contacts
- Chat: [#eevee-viewport-module](https://blender.chat/channel/eevee-viewport-module)

## Documentation
- [Code Documentation](https://wiki.blender.org/wiki/Source/Render/EEVEE)
- [EEVEE/Viewport onboarding](https://www.youtube.com/watch?v=OxwrSdt6I4w)

