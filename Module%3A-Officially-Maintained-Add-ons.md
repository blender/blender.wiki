## Links

[blender-addons](https://projects.blender.org/blender/blender-addons) repository | [Issues](https://projects.blender.org/blender/blender-addons/issues)

## Description

This project includes officially supported add-ons which have `bl_info` `support` set to "OFFICIAL".

## Status

This module is active, mostly focusing on supporting existing add-ons.
If you want to get involved, contact us (info below).

## Members

See: [Python API](https://projects.blender.org/blender/blender/wiki/Module:-Python-API-&-Text-Editor) module

## Contacts
- [#blender-coders](https://blender.chat/channel/blender-coders) on blender.chat
