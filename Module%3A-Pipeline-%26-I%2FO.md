[General Workboard](https://projects.blender.org/blender/blender/projects/10) | [USD Workboard](https://projects.blender.org/blender/blender/projects/18) | [Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=276%2c296) | [High Severity](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=276%2c285) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=276%2c298)

This project includes import/export from/to non-native file formats and integration into production pipelines.

Bug reports and patches are to be tagged with #pipeline_i_o

## Status
The module is active. If you want to get involved, contact us (info below).

## Members

Module owners: @mont29

Members:  @aras_p, @deadpin, @gaiaclary , @JulienDuroure, @makowalski, @Mysteryem

Artists:

|Subproject|Developers|
|---|---|
| File Browser | @JulianEisel |
| Images & Movies | @brecht, @Jeroen-Bakker, @Sergey, @deadpin |
| USD | @deadpin, @makowalski |
| Alembic | @deadpin |
| Collada | @gaiaclary - deprecated |
| FBX | @Mysteryem |
| glTF | @JulienDuroure |
| OBJ | @aras_p |
| PLY | @aras_p |
| STL | @aras_p |
| Python I/O extensions | @mont29 - maintenance only |

## Contacts
- [#Pipeline & I/O](https://chat.blender.org/#/room/#module-pipeline-io:blender.org) room on chat.blender.org.

## Documentation
...


## Additional content
...

