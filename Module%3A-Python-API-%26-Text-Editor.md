## Links

[Workboard](https://projects.blender.org/blender/blender/projects/12) | [Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=279%2c296) | [High Severity](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=279%2c285) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=279%2c298)

## Description

This project includes the Python API, Add-ons, the console the text editor and more.


## Status

The module is active. There are periodic development of new features and general improvements. You can see active tasks on the workboard. If you want to get involved, contact us (info below).

## Members

Module owner: @ideasman42

| |Developers|
|---|---|
| Python API | @ideasman42 @mont29 @truman @JacquesLucke @dr.sybren |
| Add-ons | @ideasman42 @mont29 @BrendonMurphy @JacquesLucke @dr.sybren |
| Console Editor | @ideasman42 |
| Text Edit | @ideasman42 |

## Contacts
- [#blender-coders](https://blender.chat/channel/blender-coders) on blender.chat