## Links

[Workboard](https://projects.blender.org/blender/blender/projects/11) | [Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=278%2c296) | [High Severity](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=278%2c285) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=278%2c298)

## Status
The module is active. There are no meetings.

## Members

Module owners: @LazyDodo @ThomasDinges

| |Developers|
|---|---|
|Windows|@LazyDodo|
|Windows ARM64| @Anthony-Roberts|
|macOS|@Brainzman @Sergey |
|Linux|@ZedDB @mont29 @ideasman42|
|Build System|@ideasman42|
|Buildbot|@bartvdbraak @sergey|
|Buildbot Machines|@bartvdbraak @Arnd|

## Contacts
- [#blender-coders](https://blender.chat/channel/blender-coders) on blender.chat