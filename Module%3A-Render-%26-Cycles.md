## Links

[Workboard](https://projects.blender.org/blender/blender/projects/13) | [Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=280%2c296) | [High Severity](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=280%2c285) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=280%2c298)

## Status
The module is active. There are bi-weekly meetings and regular work on new features and improvements

## Members

|Role| |
|---|---|
|Module Owner|@sergey @brecht|
|Artists|@juang3d @jonimercado @MetinSeven-1|

|Subproject|Developers|
|---|---|
|Cycles|@sergey @brecht @ThomasDinges @LukasStockner @weizhen|
|Cycles OptiX|@pmoursnv|
|Cycles HIP|@salipour|
|Cycles oneAPI|@xavierh @Sirgienko|
|Cycles Metal|@Michael-Jones|
|Freestyle| |
|Render Pipeline|@sergey @brecht|
|Baking|@sergey @brecht|
|Color Management|@sergey @brecht|


## Contacts
- Chat: [#render-cycles-module](https://blender.chat/channel/render-cycles-module)
- Forum: [Cycles Development](https://devtalk.blender.org/c/cycles)

## Documentation

* [Code Documentation](https://wiki.blender.org/wiki/Source/Render/Cycles)