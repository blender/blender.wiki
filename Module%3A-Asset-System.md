## Description

This module covers asset library functionality (like catalogs, asset storage, indexing), asset UIs (asset browser and asset shelf) and asset bundles provided by Blender.

## Status

This module is in active development, mostly as part of ongoing projects. Current main project is the [Remote Asset Libraries](https://projects.blender.org/blender/blender/projects/707).

Previous projects:
* [Brush Assets & Asset Shelf](https://projects.blender.org/blender/blender/projects/30) - Initially released in 4.3
* [Essentials Asset Bundle](https://projects.blender.org/blender/blender/issues/103620) - Initially released in 3.5
* [Procedural hair nodes](https://projects.blender.org/blender/blender/issues/103730) (based on node group assets) - Initially released in 3.4
* [Pose Library](https://docs.blender.org/manual/en/latest/animation/armatures/posing/editing/pose_library.html) - Initially released in 3.0
* [Asset Browser project](https://projects.blender.org/blender/blender/issues?labels=337) - Initially released in 3.0

## Blog Posts

Various core designs are explained in blog posts (from oldest to newest):
* [Asset Manager](https://code.blender.org/2020/03/asset-manager/)
* [Asset Browser Project Update](https://code.blender.org/2021/06/asset-browser-project-update/)
* [Asset Browser Workshop Outcomes](https://code.blender.org/2021/06/asset-browser-workshop-outcomes/)

## Developer Documentation

* [Asset System](https://wiki.blender.org/wiki/Source/Architecture/Asset_System) - The technical foundation for all asset functionality (backend + frontend).
