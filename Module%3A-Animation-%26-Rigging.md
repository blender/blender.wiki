## Links

[Workboard](https://projects.blender.org/blender/blender/projects/1) | [Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=268%2c296) | [High Severity](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=268%2c285) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=268%2c298)

## Description

This project includes the graph editor, dopespheet editor, NLA editor, keyframes, drivers, constraints, armatures, and more.

## Tracker Preference

The Animation & Rigging module uses the `Module: Animation & Rigging` label for both issues and pull requests. The 'Project' field (aka the workboard) is only used for things that are for tracking on the workboard (i.e. not for quick fixes and other expected-to-be-short-lived things).

## Status

The module is working on [Animation 2025](https://code.blender.org/2022/11/the-future-of-character-animation-rigging/), as well as polishing existing features and resolving bugs.

### Systems in 'Maintenance Mode'

These parts of the animation system are considered to be in 'maintenance mode'. They will not get new features. Bugs may be fixed if they do not require too much of a restructuring of the code.

- **Animation Baking**: The current system has gotten too hairy, and needs a rewrite. Relevant issues are:
    - [#79670: Baking animation of bones affected by a curve (spline IK) is wrong](https://projects.blender.org/blender/blender/issues/79670)
    - [#82168: Improvements to "Bake Action..." Operator](https://projects.blender.org/blender/blender/issues/82168)
    - [#82782: Smart Bake option for "Bake Action"](https://projects.blender.org/blender/blender/issues/82782)
    - [#85050: Baking animation on 2s ( Frame Step = 2) and with 'Overwrite Current Action' enabled, still keeps old keyframes inbetween steps.](https://projects.blender.org/blender/blender/issues/85050)
    - [#96459: NLA Visual Bake of Actions (with Rigify character) Gives broken results.](https://projects.blender.org/blender/blender/issues/96459)
    - [#109528: Bake actions operator converting F-curves to linear](https://projects.blender.org/blender/blender/issues/109528)
    - [#116484: Euler rotation is wrapped to 180 degrees when Baking Animations](https://projects.blender.org/blender/blender/issues/116484)
    - [#117972: It is possible to uncheck both Pose and Object in Bake Action and it results in an error](https://projects.blender.org/blender/blender/issues/117972)
- **Non-Linear Animation (NLA) Editor**: the system has many quirks, and is hard to work with (for both users & developers). The NLA is intended to be fully replaced by [layered Actions](https://developer.blender.org/docs/features/animation/animation_system/layered/) in the future. In the [2025-02-06 module meeting](https://devtalk.blender.org/t/2025-02-07-animation-rigging-module-meeting/38935) it was decided that developer time should be spent on that, rather than on the NLA.
    - Bugs in the current functionality will be fixed, but to a limit.
    - Fixes that require a considerable reimplementation or redesign of the NLA will not be addressed.
    - Tools that currently do not support interaction with the NLA (for example #122620) will not be extended to add that support.

## Members

Module owner: @dr.sybren

Developers:  @angavrilov, @amelief, @chrislend, @cmbasnett, @nrupsis, @PaoloAcampora, @dr.sybren, @nathanvegdahl

Artists: @BassamKurdali, @BClark, @zanqdo, @Mets, @hjalti,  @JasonSchleifer, @jpbouza-4, @LucianoMunoz, @EosFoxx

| Subproject                        | Developers                                |
| --------------------------------- | ----------------------------------------- |
| Animation Editors & Tools         | @angavrilov, @chrislend, @dr.sybren, @sergey |
| Inverse Kinematics                | @brecht                                   |
| Constraints                       | @angavrilov                               |
| Non-Linear Animation (NLA) Editor |                                           |

### Contacts

- [#animation-module](https://blender.chat/channel/animation-module) on blender.chat.
- [Forum](https://devtalk.blender.org/c/animation-rigging/21) with meeting agenda/notes and some other topics. There is also a [meeting calendar](https://stuvel.eu/anim-meetings/).
- Bug reports and patches are to be tagged with 'module > Animation & Rigging'

## Documentation

* [Module Documentatino](https://developer.blender.org/docs/features/animation/module/) for things that aren't tracked here (ideas for the future/bigger project ideas, code documentation, approach to code reviews, etc.)
* [Issues](/issues?state=open&labels=268)
* [Pull Requests](/pulls?state=open&labels=268)


<!--

##### Workboard Columns

The [workboard](/project/board/5/?order=priority) is split up into the following columns:

| Column | Description
| -----  | -----
| Backlog | Default column for new tasks. These tasks were not yet assigned to a column.
| Short Term | Tasks that were chosen to be performed in a short term (ca. 2 weeks) during a module meeting.
| Bugs | Bug reports that are intended to be fixed within 6 months.
| TODOs | Concrete tasks that have been approved and can be picked up by anyone to work on. A [maximum of 10 such tasks](https://devtalk.blender.org/t/2020-10-29-animation-rigging-module/15933) will be enforced soon.
| Known Issues | Bug reports that have been confirmed, but which are not actively worked on or expected to be fixed within 6 months.
| Design | Tasks that have conceptually been approved by the module and need design discussions for UI/UX and code. Such tasks can either be converted to a TODO or spawn one or more TODO sub-tasks.  See [Ingredients of a Patch](https://wiki.blender.org/wiki/Process/Contributing_Code#Ingredients_of_a_Patch), as that list could also help in writing design tasks.
| Design done, Low priority | Design tasks that are pretty much done, but don’t have high priority when it comes to implementing them.
| Needs Investigation | for tasks that need investigation to come to a decision whether to actually continue with it or not. This is mostly meant for already-existing tasks that look interesting, but that need an actual time investment before a decision can be made.
| Responsibility of Other Module | Tasks that are related to Animation & Rigging, but are expected to be handled by a different module.

-->