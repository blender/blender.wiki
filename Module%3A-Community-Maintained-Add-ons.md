## Links

[blender-addons](https://projects.blender.org/blender/blender-addons) repository | [blender-addons-contrib](https://projects.blender.org/blender/blender-addons-contrib) repository | [Issues](https://projects.blender.org/blender/blender-addons/issues)

## Description

This project includes officially supported add-ons which have `bl_info` support set to "COMMUNITY".

## Status

Individual community members are responsible for maintaining their own add-ons.

## Members

See: [Python API](https://projects.blender.org/blender/blender/wiki/Module:-Python-API-&-Text-Editor) module

## Contacts
- [#blender-coders](https://blender.chat/channel/blender-coders) on blender.chat

## Documentation

- [Add-ons Process](http://wiki.blender.org/index.php/Dev:Doc/Process/Addons): how to get your add-on included and maintain it.
