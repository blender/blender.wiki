## Links

[Workboard](https://projects.blender.org/blender/blender/projects/14) | [Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=281%2c296%2c290) | [High Severity Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=281%2c285%2c290) | [Needs Developer Info](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=281%2c292) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=281%2c298) | [Good First Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=281%2c302) | [Design](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=281%2c290%2c297) | [To Do](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=281%2c290%2c301) | [Pull Requests](https://projects.blender.org/blender/blender/pulls?q=&type=all&sort=&state=open&labels=281) | [Module Interest](https://projects.blender.org/blender/blender/issues?labels=400)

[Wiki](https://archive.blender.org/wiki/2024/wiki/Modules/Sculpt-Paint-Texture.html): Contains info on how to contribute (Developer, Artist or otherwise), code documentation and ideas for future projects.

## Description

This project includes sculpting, vertex and image painting.

## Status
The module is active but in a transitional period without an official module owner.

There are bi-weekly meetings on Tuesday 4pm CEST 
Active tasks are visible on the [[https://projects.blender.org/blender/blender/projects/14| workboard]].

If you want to get involved, contact us (info below) and check out the [[https://wiki.blender.org/wiki/Modules/Sculpt-Paint-Texture/Get_Involved| Get Involved ]] page.

## Members
**Developers**:  @Sergey, @HooglyBoogly, @ideasman42, @Jeroen-Bakker, @mont29, @Sean-Kim   
**Artists**: @DanielBystedt, @JulienKaspar

## Contacts
* Chat: [[https://matrix.to/#/#module-sculpt-paint-texture:blender.org| Sculpt, Paint & Texture ]] on chat.blender.org
* Bug reports and patches are to be tagged with #sculpt_paint_texture

## Additional Content
- Long Term Design: [[https://code.blender.org/2021/06/asset-creation-pipeline-design/| Asset Creation Pipeline Design ]]

### Long Term Projects
- [Texture Painting](https://projects.blender.org/blender/blender/issues/96225)

## Workboard Columns

The [workboard](https://projects.blender.org/blender/blender/projects/14) is split up into the following columns:

| Column              | Description |
| ------------------  | ----------- |
| In Queue / Upcoming | Short list of tasks to be worked on within the coming releases |
| In Progress         | Tasks / PRs that are currently being actively worked on, contains both larger "meta" tasks and individual items |
| In Review           | Pull requests that need code review or have been reviewed and need further changes. |
| Inactive            | Inactive projects and long term to do's. These are slowly transitioned to the [Wiki](https://wiki.blender.org/wiki/Modules/Sculpt-Paint-Texture). |
