## Links


[Compositing](https://projects.blender.org/blender/blender/projects/2) | [Motion Tracking](https://projects.blender.org/blender/blender/projects/8) | [Video Sequencer](https://projects.blender.org/blender/blender/projects/17)

[Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=284%2c296) | [High Severity](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=284%2c285) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=284%2c298)


## Description

This project includes the video sequencer, compositor, motion tracking, Libmv, audio.

Bug reports and patches are to be tagged with `Module: VFX & Video`.

### Status

The module is active. You can see active tasks on the workboards. If you want to get involved, contact us (info below).

## Members

* Module Owner: @sergey
* Artists: @SeanKennedy, @sebastian_k, @Andy_Goralczyk, @PabloVazquez

| |Developers|
|---|---|
|Audio|@nexyon|
|Compositing|@OmarEmaraDev, @sergey, @zazizizou |
|Libmv|@sergey, @keir|
|Motion Tracking|@sergey|
|Video Sequencer|@iss, @aras_p, @eliphaz|

**Contacts**
- Compositor module chat [#compositor-module](https://blender.chat/channel/compositor-module)
- Sequencer module chat [#sequencer-module](https://blender.chat/channel/sequencer-module)
- [#blender-coders](https://blender.chat/channel/blender-coders) on blender.chat.