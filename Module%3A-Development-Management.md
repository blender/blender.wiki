## Links

[Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=270)

## Description

This project includes feature / code / design process, release, bug tracker, communication, documentation, forum, new contributors / on-boarding, infrastructure and more.

Bug reports and patches are to be filed against #development_management

## Status

The module is active.

|Role|Developers|
|---|---|
| Communication, HQ developers coordination | [Fiona Cohen](https://projects.blender.org/FoxGlove) |
| Releases, Onboarding, Module coordination, GSoC | [Thomas Dinges](https://projects.blender.org/ThomasDinges) |
| Dev-Ops | [Arnd Marijnissen](https://projects.blender.org/Arnd) |

## Contacts

- [#blender-coders](https://blender.chat/channel/blender-coders) on blender.chat