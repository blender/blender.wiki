## Links

[Workboard](https://projects.blender.org/blender/blender/projects/7) | [Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=274%2c296) | [High Severity](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=274%2c285) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=274%2c298)

## Description
This project includes modeling tools such as meshes, modifiers, nurbs, curves, metaballs, transform, UV Editor, OpenSubdiv and more.

### Status

The module is active. There are periodic development of new features and general improvements. You can see active tasks on the workboard. If you want to get involved, contact us (info below).

### Members
* Module owners: @ideasman42 @howardt
* Module members: @mano-wii
* Artists: @DanielBystedt @jlampel @blengine @nickberckley

| |Developers|
|---|---|
| Mesh Edit Operators, Modifiers | @ideasman42 @howardt |
| NURBS, Curves, Text | @ideasman42  @sergey |
| Metaballs | Not actively maintained |
| Transform & Snap | @ideasman42 @mano-wii |
| UV Editing | @brecht @ideasman42 |
| OpenSubdiv | @sergey |

### Contacts
- [#modeling-module](https://blender.chat/channel/modeling-module) on blender.chat