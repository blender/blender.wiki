[Core Workboard](https://projects.blender.org/blender/blender/projects/3) | [Bugs](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=269%2c296) | [High Severity](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=269%2c285) | [Known Issues](https://projects.blender.org/blender/blender/issues?q=&type=all&sort=&state=open&labels=269%2c298)

## Description

This project includes low-level modules at the core of Blender: DNA & RNA, .blend file format, undo system, core kernel code (ID management), Linking/Appending/Overrides of external data, the windowmanager and editors design, and general support libraries.

## Status
The module is active. There is regular work on new features and improvements. You can see active tasks on the [workboard](https://projects.blender.org/blender/blender/projects/3). If you want to get involved, contact us (info below).

## Members

Module owners:  @mont29, @brecht, @ideasman42, @Sergey

Members: @JacquesLucke, @dr.sybren, @Jeroen-Bakker

| Sub-project | Developers |
|---|---|
| Blender Read & Write File | @mont29 |
| Append & Link & Overrides | @mont29 |
| Data-Blocks Management | @mont29 |
| Dependency Graph | @Sergey, @dr.sybren, @JacquesLucke |
| DNA & RNA | @ideasman42, @mont29, @brecht |
| Undo | @ideasman42 |
| Global Undo (aka. memfile undo) | @mont29 |

## Contacts

- [#Core](https://chat.blender.org/#/room/#module-core:blender.org) room on chat.blender.org.


## Documentation

[Blender Dev Documentation](https://developer.blender.org/docs/features/core/)